var indexmlify = require('./');

//----------------------------------------------------------
// General Tests
//----------------------------------------------------------
test('handles uri style paths', () => {
    expect(indexmlify('https://www.example.com/awesome.html')).toBe('https://www.example.com/awesome/index.html');
    expect(indexmlify('https://www.example.com/awesome/index.html')).toBe('https://www.example.com/awesome/index.html');
    expect(indexmlify('https://www.example.com/awesome')).toBe('https://www.example.com/awesome');
});

test('handles unix style paths', () => {
    expect(indexmlify('/home/user123/project/epic.html')).toBe('/home/user123/project/epic/index.html');
    expect(indexmlify('/home/user123/project/epic/index.html')).toBe('/home/user123/project/epic/index.html');
    expect(indexmlify('/home/user123/project/epic')).toBe('/home/user123/project/epic');
});

test('handles Windows style paths', () => {
  expect(indexmlify('C:\\Program Files\\Nerd\\Alert.html')).toBe('C:\\Program Files\\Nerd\\Alert\\index.html');
  expect(indexmlify('C:\\Program Files\\Nerd\\Alert\\index.html')).toBe('C:\\Program Files\\Nerd\\Alert\\index.html');
  expect(indexmlify('C:\\Program Files\\Nerd\\Alert')).toBe('C:\\Program Files\\Nerd\\Alert');
});

test('handles super funky paths', () => {
  expect(indexmlify('Super__FunKY-Path.index.txt')).toBe('Super__FunKY-Path.index/index.txt');
});

test('handles duplicate nesting paths', () => {
  expect(indexmlify('/about/me/about.md')).toBe('/about/me/about/index.md');
});

//----------------------------------------------------------
// Option Tests
//----------------------------------------------------------
test('handles separator option', () => {
  expect(indexmlify('plugin-one.md', { separator: '_DEV/' })).toBe('plugin-one_DEV/index.md');
  expect(indexmlify('plugin-one.md', { separator: '' })).toBe('plugin-oneindex.md');
});

test('handles extension option', () => {
  expect(indexmlify('about.md', { extension: 'html' })).toBe('about/index.html');
  expect(indexmlify('about.md', { extension: '' })).toBe('about/index');
});

test('handles reverse option', () => {
  expect(indexmlify('about/index.html', { reverse: true })).toBe('about.html');
  expect(indexmlify('about/index.html', { reverse: true, extension: 'md' })).toBe('about.md');
});

test('handles noIndex option', () => {
  expect(indexmlify('about.md', { noIndex: true })).toBe('about.md');
  expect(indexmlify('about.md', { noIndex: true, extension: 'html' })).toBe('about.html');
});

test('handles indexes', () => {
  expect(indexmlify('index.md', { extension: 'html' })).toBe('index.html');
  expect(indexmlify('index.html')).toBe('index.html');
  expect(indexmlify('index.html', { extension: 'html' })).toBe('index.html');
});
