# IndexmlifyJS

For url and path indexification.

> NB: This project is pronounced like *"index-em-el-ify"*

## What

Indexification is the process of taking a path or url string
and transforming it into one that could be served more elegantly
by a server or hosting environment.

For example, when you visit a website `https://some-website.com`
the server will actually serve up the file `https://some-website.com/index.html`

This usually extends further into the structure (for static sites at least), such as:

`~/docs --> ~/docs/index.html`  
`~/deep/nesting --> ~/deep/nesting/index.html`

When creating content is it often difficult navigating an IDE or text editor
when all files are named the same. For this reason, it is more productive to
name them how they sit and then transform them into cleaner url paths during processsing.

Indexmlify makes this file-to-folder name translation dead simple.

## How

```js
import indexmlify from 'indexmlify'; // or require()

const input  = '/path/to/be/indexmlified/here.xyz';
const output = indexmlify(input, { /* opts */ });

console.log(output); // --> '/path/to/be/indexmlified/here/index.xyz'

// alternative
indexmlify.resolve(input, { /* opts */ });
```

### Options

`separator`  
**Provide a custom separator for transplanting before the index.**  
This can be useful when you need to force Windows style `\` separators on single
element files that otherwise would be treated with a `/` by default.

```js
indexmlify('about.md') // --> 'about/index.md'
indexmlify('about.md', { separator: '\\' }) // --> 'about\index.md'
```

`extension`  
**Provide a custom extension rather than using the existing one.**  
This can be useful when transpiling files such as Markdown.

```js
indexmlify('about.md') // --> 'about/index.md'
indexmlify('about.md', { extension: 'html' }) // --> 'about/index.html'
```

`noIndex`  
**When true, bypasses the indexification.**  
This can be useful when you want to transform just the extention.

```js
indexmlify('about.md', { noIndex: true }) // --> 'about.md'
indexmlify('about.md', { noIndex: true, extension: 'html' }) // --> 'about.html'
```

`reverse`  
**When true, reverses the indexmlification.**  
This can be useful when analysing existing structures.

```js
indexmlify('about/index.html', { reverse: true }) // --> 'about.html'
indexmlify('about/index.html', { reverse: true, extension: 'md' }) // --> 'about.md'

// alternative
indexmlify.reverse('about/index.html', { extension: 'md' }) // --> 'about.md'
```

## Where

```js
npm install indexmlify // or
yarn add indexmlify
```

## Why

We find it useful so maybe you will too, and because sharing is caring.

## Who

Crafted with &hearts; by [DevOwt](https://devowt.com).  
Happy Coding!
