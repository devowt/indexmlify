
function indexmlify(path, opts) {
  opts = opts || {};

  return opts.reverse === true
    ? indexmlify.reverse(path, opts)
    : indexmlify.resolve(path, opts);
};


indexmlify.resolve = function(path, opts) {
  opts = opts || {};

  let separator =
    typeof opts.separator !== 'undefined'
      ? opts.separator
      : path.indexOf('\\') >= 0 ? '\\' : '/';

  let extension =
    typeof opts.extension !== 'undefined'
      ? opts.extension === '' ? '' : `.${opts.extension}`
      : '$2';

  let re = /([-\w\.]+)(\.\w+)$/i;
  let match = re.exec(path);

  if (match) {
    if (opts.noIndex === true) {
      return path.replace(re, `$1${extension}`);
    }
    else if (match[1].toLowerCase() !== 'index'){
      return path.replace(re, `$1${separator}index${extension}`);
    }
    else {
      return path.replace(re, `$1${extension}`);
    }
  }

  return path;
};


indexmlify.reverse = function(path, opts) {
  opts = opts || {};

  let extension =
    typeof opts.extension !== 'undefined'
      ? opts.extension === '' ? '' : `.${opts.extension}`
      : '$2';

  let re = /([-\w\.]+)[\\\/]index(\.\w+)$/i;
  let match = re.exec(path);

  if (match) {
    return path.replace(re, `$1${extension}`);
  }

  return path;
};

// export the module
module.exports = indexmlify;
